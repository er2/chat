
local function _loop(b, v)
  writeln(v, 'HTTP/1.1 200 OK', '')
  write(v, 'Please use telnet to use this service.')
  close(v)
end

return {
  -- submodule
  _loop = _loop,
}
