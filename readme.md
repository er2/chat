```
              __             
  ___________/__\___________ 
 /   _  _  -__ -   ___  -   \
 \__________________________/
             |  |            
             |  |            
        _____|  |______      
    ___|     |  |      |___  
   |   |     |  |      |   | 
___|___|____/____\_____|___|___

     Welcome to Er2Chat!
 You need to login to continue
Do you need input echo? [YNHQ]
```

---

This is yet another telnet chat.

This server supports both clients with \r, \n and \r\n newline.
(This means it supports `nc/telnet` and Android `ConnectBot` client)

Server currently is very simple and stupid, but can be improved.

# Running

To run this server, you need:

- **Lua** (from 5.1, supports luajit)

- **LuaSocket** (or **LuaSec**).
  `# luarocks install luasocket`

To start now, just type:
`$ lua main.lua`
