local socket = require 'socket'
local tcp = socket.bind('*', 8080)
tcp:settimeout(0)

local function get(v)
  v.buf = v.buf or ''
  local l, err
  while not err do
    l, err = v.cli:receive(1)
    if l == '\r' or l == '\n' then
      if v.le then writeln(v, '') end
      local n = v.cli:receive(1)
      if not n or n == '\n' or n == '\r'
      then l, v.buf = v.buf, nil
      else l = n end
    end
    if v.buf == nil then break
    elseif l == '\127'
    then write(v, '\008')
      v.buf = utf8.sub(v.buf, 1, -2)
    elseif l then
      v.buf = v.buf.. l
      if v.le then write(v, l) end
    end
  end
  return l, err
end

local function write(v, ...)
  v.cli:send(table.concat({...}, '\r\n'))
end

local function writeln(v, ...)
  write(v, ...)
  write(v, '\r\n')
end

local function close(v)
  v.cli:close()
  v.closed = true
end

local function _loop(b)
  local v = makev(tcp:accept(), b)
  if v then
    v.cli:settimeout(0)
    local l = v.cli:receive()
    if (l or ''):match 'GET /.* HTTP.*' then
      return 'http', v
    else init(v)
    end
  end
end

return {
  get = get,
  write = write,
  writeln = writeln,
  close = close,
  _loop = _loop,
  tcp = tcp,
}
